# Eureka Client Consumer
> 服务消费者，比较简单，通过eureka server端获取提供服务的机器集群。
>
## 一、访问请求（负载均衡）
### RestTemplate
1. 通过 http://{服务名}/接口/参数访问接口
### Ribbon 负载均衡
1. 按照配置的rule（轮询、随机...）访问机器的接口
### Feign
1. 按照接口的方式来访问，写法简单、易用

## 二、服务的高可用（设置请求超时、断路器）
### hystrix 断路器
1. 配置规则，接口访问不通时调用指定的回调方法；
2. 断路器打开规则 默认：20秒内5次失败
3. 隔离策略
    - 线程隔离：默认使用该方式，HystrixCommand将在单独的线程上执行，并发请求受到线程池中的线程数量的限制。
    - 信号量隔离：使用该方式，HystrixCommand将在调用线程上执行，开销相对较小，并发请求受到信号量个数的限制。