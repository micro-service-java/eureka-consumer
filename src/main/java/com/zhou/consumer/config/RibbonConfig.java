package com.zhou.consumer.config;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RoundRobinRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * RibbonConfig
 *
 * @author zhouxiang
 * @date 2021/12/30 3:18 下午
 */
@Configuration
public class RibbonConfig {

    @Bean
    public IRule ribbonRule() {
        // 随机
        //        return new RandomRule();
        // 轮询
        return new RoundRobinRule();
    }
}
