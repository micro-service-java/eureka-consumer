package com.zhou.consumer.config;

import feign.Logger;
import feign.Logger.Level;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * FeignConfig
 *
 * @author zhouxiang
 * @date 2021/12/30 3:40 下午
 */
@Configuration
public class FeignConfig {

    @Bean
    public Logger.Level level() {
        return Level.FULL;
    }
}
