package com.zhou.consumer.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.zhou.consumer.micro.EurekaProviderClient;
import java.net.MalformedURLException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * UserController
 *
 * @author zhouxiang
 * @date 2021/12/30 2:13 下午
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private DiscoveryClient discoveryClient;
    @Autowired
    private EurekaProviderClient eurekaProviderClient;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private LoadBalancerClient loadBalancerClient;

    @GetMapping("/feign/{id}")
    @HystrixCommand(fallbackMethod = "getByIdFallback", commandProperties = {
        @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
        @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "10000")
    }, threadPoolProperties = {
        @HystrixProperty(name = "coreSize", value = "1"),
        @HystrixProperty(name = "maxQueueSize", value = "10")
    }, threadPoolKey = "movieFallback",
        ignoreExceptions = {RuntimeException.class})
    public String getById(@PathVariable Long id) {
        // ignoreExceptions runtime类型不支持断路
        if (id % 2 == 0) {
            throw new RuntimeException("不支持偶数!");
        }
        return eurekaProviderClient.getById(id);
    }

    @GetMapping("/feign2/{id}")
    public String getById4(@PathVariable Long id) {
        log.info("feign fallback id:{}", id);
        return eurekaProviderClient.getById(id);
    }

    public String getByIdFallback(Long id) {
        System.out.println("fallback id:" + id);
        return "fallback-电影-" + id;
    }

    @GetMapping("/rest/{id}")
    public String getById2(@PathVariable Long id) {
        String applicationName = "eureka-provider-1";
        String url = "http://" + applicationName + "/movie/" + id;
        log.info("url:{}", url);
        return restTemplate.getForObject(url, String.class);
    }

    @GetMapping("/load/{id}")
    public String getById3(@PathVariable Long id) {
        String applicationName = "eureka-provider-1";
        ServiceInstance instance = loadBalancerClient.choose(applicationName);
        log.info("Scheme:{},ServiceId:{},uri:{},host:{},port:{}", instance.getScheme(), instance.getServiceId(),
            instance.getUri(), instance.getHost(), instance.getPort());
        return instance.getHost() + ":" + instance.getPort();
    }

    @GetMapping("/info")
    public List<ServiceInstance> info() {

        String applicationName = "eureka-provider-1";
        return discoveryClient.getInstances(applicationName);
    }
}
