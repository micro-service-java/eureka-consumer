package com.zhou.consumer.micro;

import com.zhou.consumer.config.FeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * EurekaProviderClient
 *
 * fallback: 进行熔断
 * fallbackFactory: 进行熔断，可以看到进行熔断的原因
 * @author zhouxiang
 * @date 2021/12/30 2:18 下午
 */
@FeignClient(name = "eureka-provider-1", configuration = FeignConfig.class,
//    fallback = EurekaProviderFallback.class,
    fallbackFactory = EurekaProviderFallbackFactory.class
)
public interface EurekaProviderClient {

    @GetMapping("/movie/{id}")
    String getById(@PathVariable Long id);
}
