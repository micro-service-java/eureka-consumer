package com.zhou.consumer.micro;

import org.springframework.stereotype.Component;

/**
 * EurekaProviderFallback
 *
 * @author zhouxiang
 * @date 2021/12/31 4:32 下午
 */
@Component
public class EurekaProviderFallback implements EurekaProviderClient {

    @Override
    public String getById(Long id) {
        return "feign-fallback-电影-" + id;
    }
}
