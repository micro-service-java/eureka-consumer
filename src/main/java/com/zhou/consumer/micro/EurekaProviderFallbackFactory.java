package com.zhou.consumer.micro;

import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * EurekaProviderFallbackFactory
 *
 * @author zhouxiang
 * @date 2021/12/31 4:37 下午
 */
@Component
public class EurekaProviderFallbackFactory implements FallbackFactory<EurekaProviderClient> {

    private static final Logger log = LoggerFactory.getLogger(EurekaProviderFallbackFactory.class);

    @Override
    public EurekaProviderClient create(Throwable cause) {
        log.error("断路，cause:{}", cause.getMessage());
        return new EurekaProviderClient() {

            @Override
            public String getById(Long id) {

                return "feign-fallback-factory-电影-" + id;
            }
        };
    }
}
